package basic;

public class Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Duck duck1 = new Duck();
		duck1.inAndOut(new O2(), new Water());
		//duck1.inAndOut(null,null);
		duck1.layEggs();
		
		Penguin pg= new Penguin(new Climate("南极"));
		pg.layEggs();
		
		WildGoose goose = new WildGoose();
		goose.layEggs();
		goose.fly();
		
		GooseGroup gg= new GooseGroup();
		WildGoose[] gooses= new WildGoose[6];
		gooses[0]=new WildGoose();
		gooses[1]=new WildGoose();
		gooses[2]=new WildGoose();
		gooses[3]=new WildGoose();
		gooses[4]=new WildGoose();
		gooses[5]=new WildGoose();
		gg.setGooses(gooses);
		gg.vFly();
		gg.yiFly();
		
		Penguin pg1= new Penguin(new Climate("南极"));
		pg1.setClimate(new Climate("赤道"));
		pg1.layEggs();
		
		TangDuck td= new TangDuck();
		td.speak();
		//1 多来几只大雁，让他们组成雁群，一会1字飞，一会V字飞。
		//2让企鹅去赤道，看他能不能下蛋！
		//3 定义一个说话接口，定义一个唐老鸭类实现此接口，让唐老鸭出来，给大家讲个笑话
		//4 预习java异常机制！
		
	}

}
