package homework;

import java.io.FileNotFoundException;

public class Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		//定义C类，在主函数中，调用B类的method方法。并处理异常。不抛出！
		
		B b = new B();
		try {
			b.method();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println("捕获了B类method方法抛出的文件未找到异常！");
		} catch(ArithmeticException e){
			System.out.println("C类捕获了B类method方法抛出的算术异常！");
		}
		System.out.println("主函数继续运行！");
	}

}
