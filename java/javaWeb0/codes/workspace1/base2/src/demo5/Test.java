package demo5;

public class Test {
	public static void main(String[] args){
		Dog dog1 = new Dog();
		//dog1.shout();
		Dog dog2 = new Dog("大花");
		//dog2.shout();
		Dog dog3 = new Dog("大狗");
		dog3.setShoutNum(9);
		//dog3.shout();
		
		Cat cat1 = new Cat();
		//cat1.shout();
		Cat cat2 = new Cat("咪咪");
		//cat2.shout();
		Cat cat3 = new Cat("大猫");
		cat3.setShoutNum(9);
		//cat3.shout();
		//Animal animal = new Animal();//Animal是一个抽象的概念！
		Monkey mon = new Monkey();
		Animal[]  group = new Animal[7];
		group[0]=dog1;
		group[1]=dog2;
		group[2]=dog3;
		group[3]=cat1;
		group[4]=cat2;
		group[5]=cat3;
		group[6]=mon;
		
		for(int i=0;i<7;i++){
			((Animal)group[i]).shout();
		}
	}
}
