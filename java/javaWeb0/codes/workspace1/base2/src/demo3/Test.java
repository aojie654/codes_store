package demo3;

public class Test {
	public static void main(String[] args){
		Dog dog1 = new Dog();
		dog1.shout();
		Dog dog2 = new Dog("大花");
		dog2.shout();
		Dog dog3 = new Dog("大狗");
		dog3.setShoutNum(9);
		dog3.shout();
		
		Cat cat1 = new Cat();
		cat1.shout();
		Cat cat2 = new Cat("咪咪");
		cat2.shout();
		Cat cat3 = new Cat("大猫");
		cat3.setShoutNum(9);
		cat3.shout();
		//Animal animal = new Animal();//Animal是一个抽象的概念！
	}
}
