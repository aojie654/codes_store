package abstractFactory;

public class Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Provider p = new SendMailFactory();
		Sender sender=p.produce();
		sender.send();
		Provider p2 = new SendWeixinFactory();
		Sender sender2=p2.produce();
		sender2.send();
	}

}
