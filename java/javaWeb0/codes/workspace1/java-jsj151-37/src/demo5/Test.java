package demo5;

public class Test {
		public static void main(String[] args){
		      //t1是对象
		Dog dog= new Dog();
		//dog.shout();
		
		Dog dog2= new Dog("wangwang");
		//dog2.shout();

		Dog dog3= new Dog("旺财");
		//dog3.shout();
		
		Cat cat1 = new Cat("kitty");
		Cat cat2 = new Cat("mimi");
		//cat1.shout();
		//cat2.shout();
		
		Monkey mon = new Monkey("金刚");
		
		Animal[] animals =new Animal[6];
		animals[0]=dog;
		animals[1]=dog2;
		animals[2]=dog3;
		animals[3]=cat1;
		animals[4]=cat2;
		animals[5]=mon;

		for(int i=0;i<6;i++){
			
			Animal ani=animals[i];
			ani.shout();
		}
		
		RoboCat rc = new RoboCat();
		rc.shout();
		rc.changeThing();
		
	}
}
