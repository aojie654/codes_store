package abstractFactory;

public class Test {

	public static void main(String[] args) {
		Provider p = new SmsSenderFactory();
		Sender s1=p.produce();
		s1.send();
		Provider p2 = new WeiSenderFactory();
		Sender s2=p2.produce();
		s2.send();
	}

}
