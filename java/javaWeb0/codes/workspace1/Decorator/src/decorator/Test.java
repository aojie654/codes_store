package decorator;

public class Test {
	public static void main(String[] args){
		//对model进行包装！然后展示！
		//需要模特！
		Model  model = new Model("张亮");
		//需要各种衣服！
		ClothDecorator sunglass = new Sunglass();//太阳镜
		ClothDecorator belt = new Belt();//皮带
		ClothDecorator t = new Tshirt();//衬衣
		ClothDecorator trouser = new Trousers();//裤子
		ClothDecorator hair = new HairStyle();//发型
		
		//进行包装！让张亮去试装！
		trouser.decorate(model);//裤子装饰了张亮！
		belt.decorate(trouser);//皮带装饰穿了裤子的张亮。
		t.decorate(belt);//上衣装饰了穿了皮带的穿了裤子的张亮！
		//hair.decorate(t);
		sunglass.decorate(t);
		
		//此时的sunglass是戴上太阳镜的留着新发型的穿了上衣皮带和裤子的张亮！ 
		//走梯台！

		sunglass.show();
		
	}
}
