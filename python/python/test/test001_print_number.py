# coding=utf-8
# @File  : test001_print_number
# @Author: aojie654
# @Date  : 2018.06.25 19:08
# @Desc  : Print Number No repeat


def print_number(num_list, length):
    """Method 1"""
    # for i in num_list:
    #     for j in num_list:
    #         for k in num_list:
    #             if i != j and j!= k and i != k:
    #                 print(str(i)+str(j)+str(k))
    """Method 2"""
    for i in range(num_list):
        num = []
        for num_var in num_list:
            if num_var not in num:
                num.append(num_var)
            if len(num) >= length:
                break
        for num_var in num:
            print(num_var, end='')


if __name__ == '__main__':
    print_number([1, 2, 3, 4], 3)
