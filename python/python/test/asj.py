# encoding=utf-8
from dictlib import dug

# meta data
data_0 = {'a0.a1.a2': 1, 'a0.a1.b2': 2, 'a0.b1.a2': 3,
          'a0.b1.b2': 4, 'b0.a1.a2': 5, 'b0.a1.b2': 6}


def conver_to_dict(data_dict):
    """
    convert meta data to dict
    """
    # convert dict to list
    dict_list = []
    for key, value in data_dict.items():
        # convert key to list
        dict_list_t = key.split('.')
        dict_dict={}
        # convert list to dict
        for cnt in range(len(dict_list_t)-1, -1, -1):
            # if the cnt is the index of -1, create the dict that including the last key and only value.
            if cnt == len(dict_list_t)-1:
                dict_dict={dict_list_t[cnt]:value}
            else:
                dict_dict={dict_list_t[cnt]:dict_dict}
        print(dict_dict)
        dict_list.append(dict_dict)
    return dict_list

def dict_merge(data_dict):
    dict_t = {}
    for key, value in data_dict.items():
        dug(dict_t, key, value)
    return dict_t

list_final = dict_merge(data_0)
print(list_final)
# # makeup
# def makup(list_t, pos, value):
#     if pos == len(list_t)-1:
#         return {list_t[pos]:value}
#     return {list_t[pos]:makup(list_t, pos+1, value)}


# # 结果数据集
# result_0 = {}
# print(type(data_0))
# for key_t, value_t in data_0.items():
#     key_list_t = str(key_t).split('.')
#     if len(key_list_t) >= 2:
#         pos=0
#         result_0[key_list_t[0]] = makup(key_list_t[1:], pos, value_t)
# print(result_0)
