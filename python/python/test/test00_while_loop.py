# coding=utf-8
# @File  : Test00_while_loop.py
# @Author: aojie654
# @Date  : 18-6-8
# @Desc  : Test While loop

while input("Please input !q to exit!") != "!q":
    print("In the while loop")
print("Out of loop")
