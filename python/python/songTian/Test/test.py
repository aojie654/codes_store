# CaesarCode.py

charsLow = "abcdefghijklmnopqrstuvwxyz"
charsUp = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
charsAll = charsLow + charsUp
i = ""

print("\nThe source chars: ")
for i in charsAll:
    print(i + "\t", end="")

print("\n\nThe source Unicode: ")

for i in charsAll:
    print("" + str(ord(i)) + "\t", end="")

print("\n\nThe Caesar Char: ")

for i in charsAll:
    if i in charsUp:
        print("" + chr((ord(i) + 3 - 65) % 26 + 65) + "\t", end="")
    elif i in charsLow:
        print("" + chr((ord(i) + 3 - 97) % 26 + 97) + "\t", end="")

print("\n\nThe Caesar Char: ")

for i in charsAll:
    if i in charsUp:
        print("" + str((ord(i) + 3) % 26 + 65) + "\t", end="")
    elif i in charsLow:
        print("" + str((ord(i) + 3) % 26 + 97) + "\t", end="")
